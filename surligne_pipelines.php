<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Traiter var_recherche ou le referrer pour surligner les mots
 *
 * Surligne les mots de la recherche (si var_recherche est présent)
 * ou des réferers dans un texte HTML
 *
 * @pipeline affichage_final
 *
 * @param string $texte Contenu de la page envoyée au navigateur
 * @return string         Contenu de la page envoyée au navigateur
 **/
function surligne_affichage_final($texte) {
	if (!$GLOBALS['html']) {
		return $texte;
	}
	$rech = _request('var_recherche');
	if (!$rech && !isset($_SERVER['HTTP_REFERER'])) {
		return $texte;
	}
	include_spip('inc/surligne');

	if (isset($_SERVER['HTTP_REFERER'])) {
		$_SERVER['HTTP_REFERER'] = preg_replace(',[^\w\,/#&;:=.?-]+,', ' ', (string) $_SERVER['HTTP_REFERER']);
	}
	if ($rech) {
		$rech = preg_replace(',[^\w\,/#&;:-]+,', ' ', (string) $rech);
	}

	return surligner_mots($texte, $rech);
}
